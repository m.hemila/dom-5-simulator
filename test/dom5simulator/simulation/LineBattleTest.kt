package dom5simulator.simulation

import dom5simulator.game.*
import io.mockk.every
import io.mockk.mockkObject
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class LineBattleTest {
    private val dummyStats = CombatAttributes(
        HitPoints(1),
        6,
        Strength(0),
        Attack(0),
        Defence(0),
        Protection(0)
    )
    private val smallStats = CombatAttributes(
        HitPoints(14),
        2,
        Strength(0),
        Attack(0),
        Defence(0),
        Protection(0)
    )
    private val highHitPointsStats = CombatAttributes(
        HitPoints(21),
        6,
        Strength(0),
        Attack(0),
        Defence(0),
        Protection(0)
    )
    private val dummyWeapon = OneHandedWeapon(Damage(0), Attack(0), Defence(0))
    private val oneDamageWeapon = OneHandedWeapon(Damage(1), Attack(1), Defence(0))

    private val dummies = UnitTemplate("Dummy", dummyStats, weapon = dummyWeapon)
    private val smallies = UnitTemplate("Small", smallStats, weapon = dummyWeapon)
    private val hpDummies = UnitTemplate("Hp Dummy", highHitPointsStats, weapon = dummyWeapon)
    private val winners = UnitTemplate("Winners", dummyStats, weapon = oneDamageWeapon)

    @BeforeEach
    fun beforeTests() {
        mockkObject(RNG)
        every { RNG.drn() } returns 0
        every { RNG.random(any()) } returns 0
    }

    @Test
    fun `Zero attackers and defenders should result in defender victory`() {
        val dummyArmy = Pair(dummies, 0)
        val winnerArmy = Pair(winners, 0)

        val battle = LineBattle(winnerArmy, dummyArmy)
        val report = battle.runBattle()

        assertEquals(Side.DEFENDER, report.winner)
    }

    @Test
    fun `Line battle should return report`() {
        val dummyArmy = Pair(dummies, 3)
        val winnerArmy = Pair(winners, 1)

        val battle = LineBattle(winnerArmy, dummyArmy)
        val report = battle.runBattle()

        assertEquals(Side.ATTACKER, report.winner)
        assertEquals(3, report.rounds)
        assertEquals(1, report.attackersLeft)
        assertEquals(0, report.defendersLeft)
    }

    @Test
    fun `There should be a limit on number of units making an attack against single opponent`() {
        val hpArmy = Pair(hpDummies, 1)
        val moreWinnerArmy = Pair(winners, 21)
        val battle = LineBattle(moreWinnerArmy, hpArmy)

        val report = battle.runBattle()

        assertEquals(3, report.rounds)
    }

    @Test
    fun `Line battle should handle smaller sizes`() {
        val smallArmy = Pair(smallies, 1)
        val moreWinnerArmy = Pair(winners, 7)
        val battle = LineBattle(moreWinnerArmy, smallArmy)

        val report = battle.runBattle()

        assertEquals(2, report.rounds)
    }

    @Test
    fun `should work when bigger side one shots`() {
        val dummyArmy = Pair(dummies, 3)
        val winnerArmy = Pair(winners, 5)

        val battle = LineBattle(winnerArmy, dummyArmy)
        val report = battle.runBattle()

        assertEquals(Side.ATTACKER, report.winner)
        assertEquals(1, report.rounds)
        assertEquals(5, report.attackersLeft)
        assertEquals(0, report.defendersLeft)
    }

    @Test
    fun `should end with defender winning when running out of round`() {
        val dummyArmy = Pair(hpDummies, 5)
        val winnerArmy = Pair(winners, 1)

        val battle = LineBattle(winnerArmy, dummyArmy)
        val report = battle.runBattle()

        assertEquals(Side.DEFENDER, report.winner)
        assertEquals(100, report.rounds)
        assertEquals(1, report.attackersLeft)
        assertEquals(1, report.defendersLeft)
    }
}
