package dom5simulator.game

import io.mockk.every
import io.mockk.mockkObject
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class DomUnitTest {
    private val basicCombatAttributes = CombatAttributes(
        HitPoints(10),
        2,
        Strength(10),
        Attack(10),
        Defence(10),
        Protection(0)
    )
    private val naturalProtectionStats = CombatAttributes(
        HitPoints(10),
        4,
        Strength(10),
        Attack(10),
        Defence(10),
        Protection(4)
    )
    private val basicHeadgear = Headgear(Protection(10), Defence(-1))
    private val basicBodyArmor = BodyArmor(Protection(10), Defence(-1))
    private val spear = OneHandedWeapon(Damage(3), Attack(1), Defence(0))
    private val broadSword = OneHandedWeapon(Damage(6), Attack(1), Defence(1))
    private val paperShield = Shield(Protection(2), Defence(4))

    private val militiaTemplate = UnitTemplate("Militia", basicCombatAttributes, basicHeadgear, basicBodyArmor, spear)
    private val spearmanTemplate =
        UnitTemplate("Spearman", basicCombatAttributes, basicHeadgear, basicBodyArmor, spear, paperShield)
    private val swordsmanTemplate =
        UnitTemplate("Swordsman", basicCombatAttributes, basicHeadgear, basicBodyArmor, broadSword, paperShield)
    private val unitWithNaturalProtectionTemplate =
        UnitTemplate("Nat Prot Unit", naturalProtectionStats, basicHeadgear, basicBodyArmor, spear)

    private var militia = militiaTemplate.createUnit()
    private var opponentMilitia = militiaTemplate.createUnit()
    private var spearman = spearmanTemplate.createUnit()
    private var swordsman = swordsmanTemplate.createUnit()
    private var unitWithNaturalProtection = unitWithNaturalProtectionTemplate.createUnit()

    @BeforeEach
    fun beforeTests() {
        mockkObject(RNG)
        every { RNG.drn() } returns 0

        militia = militiaTemplate.createUnit()
        opponentMilitia = militiaTemplate.createUnit()
        spearman = spearmanTemplate.createUnit()
        swordsman = swordsmanTemplate.createUnit()
        unitWithNaturalProtection = unitWithNaturalProtectionTemplate.createUnit()
    }

    @Test
    fun `should be able to deal damage`() {
        militia.makeAnAttack(opponentMilitia)

        assertEquals(7, opponentMilitia.currentHitPoints.hitPoints)
    }

    @Test
    fun `should deal twice the damage with two attacks`() {
        militia.makeAnAttack(opponentMilitia)
        militia.makeAnAttack(opponentMilitia)

        assertEquals(4, opponentMilitia.currentHitPoints.hitPoints)
    }

    @Test
    fun `should deal less damage against shield`() {
        militia.makeAnAttack(spearman)
        militia.makeAnAttack(spearman)

        assertEquals(8, spearman.currentHitPoints.hitPoints)
    }

    @Test
    fun `should be dead after instant kill`() {
        spearman.takeDamage(Damage(999))

        println(spearman.currentHitPoints)
        println(spearman.currentHitPoints.hitPoints <= 0)
        assertTrue(spearman.isDead)
    }

    @Test
    fun `natural protection and armor should have diminishing returns`() {
        swordsman.makeAnAttack(unitWithNaturalProtection)

        assertEquals(7, unitWithNaturalProtection.currentHitPoints.hitPoints)
    }
}
