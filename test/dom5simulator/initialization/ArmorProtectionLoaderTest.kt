package dom5simulator.initialization

import dom5simulator.game.Protection
import io.mockk.unmockkAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals

internal class ArmorProtectionLoaderTest {
    @BeforeEach
    fun reset() {
        unmockkAll()
        ArmorProtectionLoader.reset()
    }

    @Test
    fun `should load protections`() {
        val file = File("test/test-data")
        val protections = ArmorProtectionLoader.load(file)

        assertEquals(Protection(16), protections[2])
        assertEquals(Protection(18), protections[13])
        assertEquals(Protection(21), protections[21])
    }
}
