package dom5simulator.initialization

import dom5simulator.game.Attack
import dom5simulator.game.Damage
import dom5simulator.game.Defence
import dom5simulator.game.OneHandedWeapon
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals

internal class WeaponLoaderTest {
    @BeforeEach
    fun reset() {
        unmockkAll()
        WeaponLoader.reset()
    }

    @Test
    fun `should load weapons`() {
        val damages = HashMap<Int, Damage>()
        damages[1] = Damage(3)
        damages[2] = Damage(5)
        damages[8] = Damage(6)
        mockkObject(WeaponDamageLoader)
        every { WeaponDamageLoader.load(any()) } returns damages

        val file = File("test/test-data")
        val weapons = WeaponLoader.load(file)

        assertEquals(3, weapons.size)

        val spear = weapons[1] as OneHandedWeapon
        val pike = weapons[2] as OneHandedWeapon
        val broadSword = weapons[8] as OneHandedWeapon

        assertEquals(Damage(3), spear.damage)
        assertEquals(Attack(0), spear.attack)
        assertEquals(Defence(0), spear.defence)

        assertEquals(Damage(5), pike.damage)
        assertEquals(Attack(0), pike.attack)
        assertEquals(Defence(-1), pike.defence)

        assertEquals(Damage(6), broadSword.damage)
        assertEquals(Attack(1), broadSword.attack)
        assertEquals(Defence(1), broadSword.defence)
    }

    @Test
    fun `should load weapons without damages`() {
        val damages = HashMap<Int, Damage>()
        mockkObject(WeaponDamageLoader)
        every { WeaponDamageLoader.load(any()) } returns damages

        val file = File("test/test-data")
        val weapons = WeaponLoader.load(file)

        assertEquals(3, weapons.size)

        val spear = weapons[1] as OneHandedWeapon
        val pike = weapons[2] as OneHandedWeapon
        val broadSword = weapons[8] as OneHandedWeapon

        assertEquals(Damage(0), spear.damage)
        assertEquals(Damage(0), pike.damage)
        assertEquals(Damage(0), broadSword.damage)
    }
}
