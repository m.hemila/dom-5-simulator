package dom5simulator.initialization

import dom5simulator.game.Damage
import io.mockk.unmockkAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals

internal class WeaponDamageLoaderTest {
    @BeforeEach
    fun reset() {
        unmockkAll()
        WeaponDamageLoader.reset()
    }

    @Test
    fun `should return correct damages`() {
        val file = File("test/test-data")
        val damages = WeaponDamageLoader.load(file)

        assertEquals(Damage(3), damages[1])
        assertEquals(Damage(5), damages[2])
        assertEquals(Damage(6), damages[8])
    }
}
