package dom5simulator.initialization

import dom5simulator.game.Armor
import dom5simulator.game.Defence
import dom5simulator.game.Protection
import dom5simulator.game.RNG
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals

internal class ArmorLoaderTest {
    @BeforeEach
    fun reset() {
        unmockkAll()
        ArmorLoader.reset()
    }

    @Test
    fun `should load armors`() {
        val protections = HashMap<Int, Protection>()
        protections[2] = Protection(16)
        protections[13] = Protection(18)
        protections[21] = Protection(21)
        mockkObject(ArmorProtectionLoader)
        every { ArmorProtectionLoader.load(any()) } returns protections

        val file = File("test/test-data")
        val armors = ArmorLoader.load(file)

        assertEquals(3, armors.size)

        val shield = armors[2] as Armor
        val chainMail = armors[13] as Armor
        val helmet = armors[21] as Armor

        assertEquals(Defence(3), shield.defence)
        assertEquals(Protection(16), shield.protection)
        assertEquals(Defence(-2), chainMail.defence)
        assertEquals(Protection(18), chainMail.protection)
        assertEquals(Defence(-1), helmet.defence)
        assertEquals(Protection(21), helmet.protection)
    }

    @Test
    fun `should work when protection cannot be found`() {
        val protections = HashMap<Int, Protection>()
        mockkObject(ArmorProtectionLoader)
        every { ArmorProtectionLoader.load(any()) } returns protections

        val file = File("test/test-data")
        val armors = ArmorLoader.load(file)

        assertEquals(3, armors.size)

        val shield = armors[2] as Armor
        val chainMail = armors[13] as Armor
        val helmet = armors[21] as Armor

        assertEquals(Protection(0), shield.protection)
        assertEquals(Protection(0), chainMail.protection)
        assertEquals(Protection(0), helmet.protection)
    }
}
