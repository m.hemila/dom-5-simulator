package dom5simulator.initialization

import dom5simulator.game.*
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

internal class UnitTemplateLoaderTest {
    @BeforeEach
    fun setUp() {
        unmockkAll()
        UnitTemplateLoader.reset()
        loadWeapons()
        loadArmors()
    }

    @Test
    fun `should load templates`() {
        val file = File("test/test-data")
        val templates = UnitTemplateLoader.load(file)

        val hoplite = templates[14]
        val militia = templates[18]

        assertNotNull(hoplite)
        assertNotNull(hoplite?.weapon)
        assertNotNull(hoplite?.shield)
        assertNotNull(hoplite?.bodyArmor)
        assertNotNull(hoplite?.headgear)

        assertNotNull(militia)
        assertNotNull(militia?.weapon)
        assertNotNull(militia?.shield)
        assertNotNull(militia?.bodyArmor)
        assertNotNull(militia?.headgear)
    }

    private fun loadWeapons() {
        val weapons = HashMap<Int, Weapon>()
        weapons[1] = OneHandedWeapon(Damage(1), Attack(1), Defence(1))
        weapons[28] = OneHandedWeapon(Damage(1), Attack(1), Defence(1))
        mockkObject(WeaponLoader)
        every { WeaponLoader.load(any()) } returns weapons
    }

    private fun loadArmors() {
        val armors = HashMap<Int, Armor>()
        armors[5] = BodyArmor(Protection(1), Defence(1))
        armors[119] = Headgear(Protection(1), Defence(1))
        armors[2] = Shield(Protection(1), Defence(1))
        armors[14] = BodyArmor(Protection(1), Defence(1))
        armors[123] = Headgear(Protection(1), Defence(1))
        armors[209] = Shield(Protection(1), Defence(1))
        mockkObject(ArmorLoader)
        every { ArmorLoader.load(any()) } returns armors
    }
}
