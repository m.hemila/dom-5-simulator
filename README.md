# Dominions 5 Combat simulator

This project is a simulator of tactical battles for a game called Dominions 5. Dominion 5 is a strategy game with battles taking place in tactical computer run simulation. The game relies heavily on magic and vast variety of both items and units. As there is no simulator in game I wanted to create one to test different army compositions.

The main targets for this project is to learn Kotlin, learn how to create sensible package structure/architecture for project and learn how to write "perfect code". 

This project will most likely be left unfinished as the game itself is quite complex and feature filled. Currently the simulation can be tested through IDEA for example Intellij by running the main function with proper arguments.

Example:
main(arrayOf("14", "12", "18", "60"))
 


Setting up environment:
Install openjdk-11-jdk

Project language: 
Kotlin

Dependencies:
MockK
Junit
Kotlin
