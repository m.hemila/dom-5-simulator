package dom5simulator

import dom5simulator.game.UnitTemplate
import dom5simulator.initialization.*
import dom5simulator.simulation.LineBattle
import dom5simulator.simulation.Simulator
import java.io.File

fun main(args: Array<String>) {
    println("\nDominions 5 Combat Simulator\n\n")

    if (args.size != 4) {
        error(
            "Wrong number of arguments, correct number is 4:\n" +
                    "attackingTemplate, numberOfAttackers, defendingTemplate, numberOfDefenders"
        )
    }

    val attackingTroopId = args[0].toInt()
    val numberOfAttackers = args[1].toInt()
    val defendingTroopId = args[2].toInt()
    val numberOfDefenders = args[3].toInt()

    val troopTemplates = UnitTemplateLoader.load(File("src/game-data"))

    val attackingTemplate: UnitTemplate = troopTemplates[attackingTroopId] ?: error("Chosen troop was not found")
    val defendingTemplate: UnitTemplate = troopTemplates[defendingTroopId] ?: error("Chosen troop was not found")
    val attacker = Pair(attackingTemplate, numberOfAttackers)
    val defender = Pair(defendingTemplate, numberOfDefenders)
    val battles = List(1000) { LineBattle(attacker, defender) }
    val battleReports = Simulator.runSimulations(battles)

    val finalReport = ReportMaker.createStringReport(
        attackerTemplateName = attackingTemplate.name,
        numberOfAttackers = numberOfAttackers,
        defenderTemplateName = defendingTemplate.name,
        numberOfDefenders = numberOfDefenders,
        reports = battleReports
    )
    println(finalReport)
}
