package dom5simulator

import dom5simulator.simulation.BattleReport
import dom5simulator.simulation.Side

object ReportMaker {
    fun createStringReport(
        attackerTemplateName: String,
        numberOfAttackers: Int,
        defenderTemplateName: String,
        numberOfDefenders: Int,
        reports: Collection<BattleReport>
    ): String {
        val reportString = StringBuilder()

        reportString.append("Battle Results\n")
        reportString.append("$attackerTemplateName: $numberOfAttackers vs $defenderTemplateName: $numberOfDefenders\n")
        val attackerWinCount = reports.count { it.winner == Side.ATTACKER }
        val defenderWinCount = reports.count { it.winner == Side.DEFENDER }
        reportString.append("Attacker - Defender\n")
        reportString.append("$attackerWinCount - $defenderWinCount")

        return reportString.toString()
    }
}
