package dom5simulator.initialization

import dom5simulator.game.*
import java.io.File

object ArmorLoader {
    private var lazyLoadedArmors: Map<Int, Armor> = HashMap()

    fun load(baseFolder: File): Map<Int, Armor> {
        if (lazyLoadedArmors.isNotEmpty()) return lazyLoadedArmors

        val lines = LoaderHelper.readFileLinesExcludingFirst(baseFolder, "armors.csv")
        val protections = ArmorProtectionLoader.load(baseFolder)
        val armors = createArmorsMap(lines, protections)

        lazyLoadedArmors = armors
        return armors
    }

    private fun createArmorsMap(
        lines: List<String>,
        protections: Map<Int, Protection>
    ): Map<Int, Armor> {
        val armors = HashMap<Int, Armor>()

        lines.forEach {
            val parts = it.split("\t")

            val id = parts[0].toInt()
            val protection: Protection = protections[id] ?: Protection(0)
            armors[id] = createArmor(parts, protection)
        }

        return armors
    }

    private fun createArmor(
        parts: List<String>,
        protection: Protection
    ): Armor {
        val type = parts[2].toInt()
        val defence = Defence(parts[3].toInt())

        return when (type) {
            4 -> Shield(protection, defence)
            5 -> BodyArmor(protection, defence)
            6 -> Headgear(protection, defence)
            else -> UnsupportedArmor(protection, defence)
        }
    }

    fun reset() {
        lazyLoadedArmors = HashMap()
    }
}
