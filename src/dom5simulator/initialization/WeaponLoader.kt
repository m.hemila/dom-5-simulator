package dom5simulator.initialization

import dom5simulator.game.*
import java.io.File

object WeaponLoader {
    private var lazyLoadedWeapons: Map<Int, Weapon> = HashMap()

    fun load(baseFolder: File): Map<Int, Weapon> {
        if (lazyLoadedWeapons.isNotEmpty()) return lazyLoadedWeapons

        val damages = WeaponDamageLoader.load(baseFolder)
        val lines = LoaderHelper.readFileLinesExcludingFirst(baseFolder, "weapons.csv")
        val weapons = createWeaponsMap(lines, damages)

        lazyLoadedWeapons = weapons
        return weapons
    }

    private fun createWeaponsMap(
        lines: List<String>,
        damages: Map<Int, Damage>
    ): Map<Int, Weapon> {
        val weapons = HashMap<Int, Weapon>()
        lines.forEach {
            val parts = it.split("\t")

            val id = parts[0].toInt()
            val damageId = parts[2].toInt()
            val damage = damages[damageId] ?: Damage(0)
            weapons[id] = createWeapon(parts, damage)
        }
        return weapons
    }

    private fun createWeapon(
        parts: List<String>,
        damage: Damage
    ): OneHandedWeapon {
        val attack = Attack(parts[3].toInt())
        val defence = Defence(parts[4].toInt())

        return OneHandedWeapon(damage, attack, defence)
    }

    fun reset() {
        lazyLoadedWeapons = HashMap()
    }
}
