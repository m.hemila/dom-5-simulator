package dom5simulator.initialization

import dom5simulator.game.Protection
import java.io.File

internal object ArmorProtectionLoader {
    private var lazyLoadedProtections: Map<Int, Protection> = HashMap()

    fun load(baseFolder: File): Map<Int, Protection> {
        if (lazyLoadedProtections.isNotEmpty()) return lazyLoadedProtections

        val lines = LoaderHelper.readFileLinesExcludingFirst(baseFolder, "protections_by_armor.csv")
        val protections = createProtectionMap(lines)

        lazyLoadedProtections = protections
        return protections
    }

    private fun createProtectionMap(lines: List<String>): Map<Int, Protection> {
        val protections = HashMap<Int, Protection>()

        lines.forEach {
            val parts = it.split("\t")
            val zone = parts[0].toInt()

            val lineIsForBodyArmorLegsOrArms = zone == 3 || zone == 4
            if (!lineIsForBodyArmorLegsOrArms) {
                val armorId = parts[2].toInt()
                val protection = Protection(parts[1].toInt())
                protections[armorId] = protection
            }
        }

        return protections
    }

    fun reset() {
        lazyLoadedProtections = HashMap()
    }
}
