package dom5simulator.initialization

import dom5simulator.game.Damage
import java.io.File

object WeaponDamageLoader {
    private var lazyLoadedDamages: Map<Int, Damage> = HashMap()
    fun load(baseFolder: File): Map<Int, Damage> {
        if (lazyLoadedDamages.isNotEmpty()) return lazyLoadedDamages

        val lines = LoaderHelper.readFileLinesExcludingFirst(baseFolder, "effects_weapons.csv")
        val damages = createDamageMap(lines)

        lazyLoadedDamages = damages
        return damages
    }

    private fun createDamageMap(lines: List<String>): Map<Int, Damage> {
        val damages = HashMap<Int, Damage>()

        lines.forEach {
            val parts = it.split("\t")

            val id = parts[0].toInt()
            val damage = Damage(parts[5].toInt())
            damages[id] = damage
        }

        return damages
    }

    fun reset() {
        lazyLoadedDamages = HashMap()
    }
}
