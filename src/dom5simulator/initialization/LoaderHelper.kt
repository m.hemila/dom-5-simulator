package dom5simulator.initialization

import java.io.File

internal object LoaderHelper {
    fun readFileLinesExcludingFirst(baseFolder: File, finalFile: String): List<String> {
        val file = baseFolder.resolve(finalFile)
        return file.bufferedReader().readLines().drop(1)
    }
}
