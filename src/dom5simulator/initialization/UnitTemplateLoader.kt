package dom5simulator.initialization

import dom5simulator.game.*
import java.io.File

object UnitTemplateLoader {
    private var lazyLoadedTemplates: Map<Int, UnitTemplate> = HashMap()

    fun load(baseFolder: File): Map<Int, UnitTemplate> {
        if (lazyLoadedTemplates.isNotEmpty()) return lazyLoadedTemplates

        val armors = ArmorLoader.load(baseFolder)
        val weapons = WeaponLoader.load(baseFolder)
        val lines = LoaderHelper.readFileLinesExcludingFirst(baseFolder, "BaseU.csv")
        val unitTemplates = createTemplateMap(lines, armors, weapons)

        lazyLoadedTemplates = unitTemplates
        return unitTemplates
    }

    private fun createTemplateMap(
        lines: List<String>,
        armors: Map<Int, Armor>,
        weapons: Map<Int, Weapon>
    ): Map<Int, UnitTemplate> {
        val unitTemplates = HashMap<Int, UnitTemplate>()

        lines.forEach {
            val parts = it.split("\t")
            val id = parts[0].toInt()
            unitTemplates[id] = createTemplate(parts, armors, weapons)
        }

        return unitTemplates
    }

    private fun createTemplate(
        parts: List<String>,
        armors: Map<Int, Armor>,
        weapons: Map<Int, Weapon>
    ): UnitTemplate {
        val name = parts[1]
        val combatAttributes = parseCombatAttributes(parts)
        val weapon = parseWeapons(parts, weapons)
        val unitArmors = parseArmors(parts, armors)

        return UnitTemplate(
            name,
            combatAttributes,
            weapon = weapon,
            headgear = unitArmors.first,
            bodyArmor = unitArmors.second,
            shield = unitArmors.third
        )
    }

    private fun parseCombatAttributes(parts: List<String>): CombatAttributes {
        val hitPoints = HitPoints(parts[19].toInt())
        val size = parts[17].toByte()
        val naturalProtection = Protection(parts[20].toInt())
        val strength = Strength(parts[23].toInt())
        val attack = Attack(parts[24].toInt())
        val defence = Defence(parts[25].toInt())

        return CombatAttributes(
            hitPoints,
            size,
            strength,
            attack,
            defence,
            naturalProtection
        )

    }

    // TODO: Return list of weapons instead of just an optional as there can be multiple for single unit
    private fun parseWeapons(parts: List<String>, weapons: Map<Int, Weapon>): Weapon? {
        val weaponString = parts[2]
        if (weaponString.isEmpty()) return null

        val weaponId = weaponString.toInt()
        return weapons[weaponId] ?: error("Weapon $weaponId was not found")
    }

    private fun parseArmors(parts: List<String>, armors: Map<Int, Armor>): Triple<Headgear?, BodyArmor?, Shield?> {
        var shield: Shield? = null
        var bodyArmor: BodyArmor? = null
        var headgear: Headgear? = null

        for (i in 9..13) {
            val armorString = parts[i]
            if (armorString.isEmpty()) break

            val armorId = armorString.toInt()
            val armor = armors[armorId]
            when (armor) {
                is Headgear -> headgear = armor
                is BodyArmor -> bodyArmor = armor
                is Shield -> shield = armor
            }
        }

        return Triple(headgear, bodyArmor, shield)
    }

    fun reset() {
        lazyLoadedTemplates = HashMap()
    }
}
