package dom5simulator.simulation

enum class Side {
    ATTACKER,
    DEFENDER
}
