package dom5simulator.simulation

import dom5simulator.game.DomUnit
import dom5simulator.game.RNG
import dom5simulator.game.UnitTemplate
import java.lang.Integer.min
import kotlin.math.ceil

class LineBattle(attackerArmy: Pair<UnitTemplate, Int>, defenderArmy: Pair<UnitTemplate, Int>) :
    Battle {

    private var rounds = 0

    private val attackerTemplate = attackerArmy.first
    private val defenderTemplate = defenderArmy.first
    private val numberOfAttackers = attackerArmy.second
    private val numberOfDefenders = defenderArmy.second
    private val attackersPerSquare = 6 / attackerTemplate.size
    private val defendersPerSquare = 6 / defenderTemplate.size

    private val attackersAlive = MutableList(numberOfAttackers) { attackerTemplate.createUnit() }
    private val defendersAlive = MutableList(numberOfDefenders) { defenderTemplate.createUnit() }

    override fun runBattle(): BattleReport {

        while (battleContinues()) {
            rounds++
            runRound()
        }

        return createReport()
    }

    private fun battleContinues() = !roundLimitReached() && bothSidesAlive()
    private fun roundLimitReached() = rounds >= 100
    private fun bothSidesAlive() = attackersAlive.isNotEmpty() && defendersAlive.isNotEmpty()

    private fun runRound() {
        val attackerMaxSquares = ceil(attackersAlive.size / attackersPerSquare.toDouble()).toInt()
        val defenderMaxSquares = ceil(defendersAlive.size / defendersPerSquare.toDouble()).toInt()

        if (attackerMaxSquares <= defenderMaxSquares) {
            val attackerChunks = attackersAlive.chunked(attackersPerSquare)
            val nroOfEndOfLineSquares = 2 * 3
            val surroundingSquares = attackerChunks.size + nroOfEndOfLineSquares
            val maxSurroundingDefenders = surroundingSquares * defendersPerSquare
            val defenderChunks = defendersAlive.take(maxSurroundingDefenders).chunked(defendersPerSquare)

            makeAttacks(attackerChunks, defenderChunks)
            makeAttacks(defenderChunks, attackerChunks)
        } else {
            val defenderChunks = defendersAlive.chunked(defendersPerSquare)
            val nroOfEndOfLineSquares = 2 * 3
            val surroundingSquares = defenderChunks.size + nroOfEndOfLineSquares
            val maxSurroundingAttackers = surroundingSquares * attackersPerSquare
            val attackerChunks = attackersAlive.take(maxSurroundingAttackers).chunked(attackersPerSquare)

            makeAttacks(attackerChunks, defenderChunks)
            makeAttacks(defenderChunks, attackerChunks)
        }

        attackersAlive.removeIf { it.isDead }
        defendersAlive.removeIf { it.isDead }
    }

    private fun makeAttacks(attacking: List<List<DomUnit>>, defending: List<List<DomUnit>>) {
        for (i in attacking.indices) {
            val targets = defending[i % defending.size].filter { !it.isDead }
            if(targets.isEmpty()) return
            val targetChosen = targets[RNG.random(targets.size)]
            attacking[i].forEach { it.makeAnAttack(targetChosen) }
        }
    }

    private fun createReport(): BattleReport {
        val winner: Side = if (defendersAlive.isEmpty() && attackersAlive.isNotEmpty()) Side.ATTACKER else Side.DEFENDER

        return BattleReport(
            winner = winner,
            rounds = rounds,
            attackersLeft = attackersAlive.size,
            defendersLeft = defendersAlive.size
        )
    }
}
