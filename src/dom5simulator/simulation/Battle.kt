package dom5simulator.simulation

interface Battle {
    fun runBattle(): BattleReport
}
