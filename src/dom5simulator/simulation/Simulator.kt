package dom5simulator.simulation

object Simulator {
    fun runSimulations(battles: Collection<Battle>): Collection<BattleReport> {
        return battles.map { battle -> battle.runBattle() }
    }
}
