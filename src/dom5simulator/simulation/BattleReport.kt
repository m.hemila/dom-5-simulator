package dom5simulator.simulation

data class BattleReport(
    val winner: Side,
    val rounds: Int,
    val attackersLeft: Int,
    val defendersLeft: Int
)
