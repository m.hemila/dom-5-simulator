package dom5simulator.game

import java.util.*

object RNG {
    private val random = Random()

    fun drn() = drnOnce() + drnOnce()

    private fun drnOnce(): Int {
        val i = (1..6).random()

        return if (i == 6) i + drnOnce() else i
    }

    private fun IntRange.random() =
        random.nextInt((endInclusive + 1) - start) + start

    fun random(bound: Int) = random.nextInt(bound)
}
