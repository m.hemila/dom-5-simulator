package dom5simulator.game

inline class Attack(val attack: Int) {
    operator fun plus(other: Attack): Attack = Attack(this.attack + other.attack)
}

inline class Damage(val damage: Int) {
    operator fun plus(other: Damage): Damage = Damage(this.damage + other.damage)
    operator fun plus(other: Strength): Damage = Damage(this.damage + other.strength)
}

inline class Defence(val defence: Int) {
    operator fun plus(other: Defence): Defence = Defence(this.defence + other.defence)
}

inline class HitPoints(val hitPoints: Int) {
    operator fun plus(other: HitPoints): HitPoints = HitPoints(this.hitPoints + other.hitPoints)
}

inline class Strength(val strength: Int) {
    operator fun plus(other: Strength): Strength = Strength(this.strength + other.strength)
    operator fun plus(other: Damage): Damage = Damage(this.strength + other.damage)
}

inline class Protection(val protection: Int) {
    operator fun plus(other: Protection): Protection = Protection(this.protection + other.protection)
}
