package dom5simulator.game

class UnitTemplate(
    val name: String,
    val combatAttributes: CombatAttributes,
    val headgear: Headgear? = null,
    val bodyArmor: BodyArmor? = null,
    val weapon: Weapon? = null,
    val shield: Shield? = null
) {
    fun createUnit(): DomUnit {
        return DomUnit(this)
    }

    val size = combatAttributes.size

    companion object {
        fun regularHuman(): UnitTemplate {
            val attributes = CombatAttributes(HitPoints(10), 2, Strength(10), Attack(10), Defence(10), Protection(0))
            val basicHeadgear = Headgear(Protection(10), Defence(-1))
            val basicBodyArmor = BodyArmor(Protection(10), Defence(-1))
            val spear = OneHandedWeapon(Damage(7), Attack(1), Defence(0))
            val shield = Shield(Protection(10), Defence(4))

            return UnitTemplate("Human Soldier", attributes, basicHeadgear, basicBodyArmor, spear, shield)
        }
    }
}
