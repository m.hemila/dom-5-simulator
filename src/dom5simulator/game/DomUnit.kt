package dom5simulator.game

class DomUnit(
    template: UnitTemplate
) {
    private val baseAttributes = template.combatAttributes
    private val weapon = template.weapon
    private val shield = template.shield
    private val headgear = template.headgear
    private val bodyArmor = template.bodyArmor

    var currentHitPoints: HitPoints = baseAttributes.maxHitPoints
        private set

    fun makeAnAttack(target: DomUnit) {
        check(this != target) { "Unit cannot attack itself" }
        if (weapon == null) return

        val attackRoll = calculateAttack().attack + RNG.drn()
        val defenceRoll = target.calculateDefence().defence + RNG.drn()

        if (attackRoll > defenceRoll) dealDamage(target, 0)
        else if (target.shield != null && attackRoll > defenceRoll - target.shield.parry.defence)
            dealDamage(target, target.shield.protection.protection)
    }

    private fun calculateAttack() = baseAttributes.attack + weapon!!.attack

    private fun calculateDefence(): Defence {
        val zero = Defence(0)

        return baseAttributes.defence +
                (weapon?.defence ?: zero) +
                (headgear?.defence ?: zero) +
                (bodyArmor?.defence ?: zero) +
                (shield?.defence ?: zero)
    }

    private fun dealDamage(target: DomUnit, shieldProt: Int) {
        val damageRoll = Damage(calculateDamage().damage + RNG.drn() - shieldProt)
        target.takeDamage(damageRoll)
    }

    fun takeDamage(damage: Damage) {
        val protectionRoll = calculateProtection().protection + RNG.drn()
        val difference = damage.damage - protectionRoll

        if (difference > 0) {
            val hp = currentHitPoints.hitPoints
            currentHitPoints = HitPoints(hp - difference)
        }

    }

    private fun calculateDamage() = baseAttributes.strength + weapon!!.damage

    private fun calculateProtection(): Protection {
        val natProt = baseAttributes.naturalProtection.protection
        val armorProt = bodyArmor?.protection?.protection ?: 0

        return Protection(natProt + armorProt - (natProt * armorProt / 40))
    }

    val isDead: Boolean
        get() = currentHitPoints.hitPoints <= 0
}
