package dom5simulator.game

data class CombatAttributes(
    val maxHitPoints: HitPoints,
    val size: Byte,
    val strength: Strength,
    val attack: Attack,
    val defence: Defence,
    val naturalProtection: Protection
)
