package dom5simulator.game

interface Armor {
    val protection: Protection
    val defence: Defence
}

interface OneHand
interface Weapon {
    val damage: Damage
    val attack: Attack
    val defence: Defence
}

sealed class Equipment

data class Headgear(
    override val protection: Protection,
    override val defence: Defence
) : Equipment(), Armor

data class BodyArmor(
    override val protection: Protection,
    override val defence: Defence
) : Equipment(), Armor

data class Shield(
    override val protection: Protection,
    override val defence: Defence
) : Equipment(), OneHand, Armor {
    val parry = defence
}

data class OneHandedWeapon(
    override val damage: Damage,
    override val attack: Attack,
    override val defence: Defence
) : Equipment(), Weapon, OneHand

data class UnsupportedArmor(
    override val protection: Protection,
    override val defence: Defence
) : Equipment(), Armor
